<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BasketController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\StripePaymentController;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/fill-db', function() {
    \Artisan::call('migrate:refresh --seed');
    return "successfully!";
})->name('fill-db');
Route::get('/dashboard', [ProductController::class, 'index'])->middleware(['auth'])->name('dashboard');

Route::get('/basket/list', [BasketController::class, 'index'])->middleware(['auth'])->name('basket.index');
Route::post('/basket/add', [BasketController::class, 'create'])->middleware(['auth'])->name('basket.add');

Route::get('/orders', [OrderController::class, 'index'])->middleware(['auth'])->name('order.index');
Route::post('/orders/add', [OrderController::class, 'store'])->middleware(['auth'])->name('order.store');

Route::get('pay', [StripePaymentController::class, 'pay'])->name('payment');
Route::post('pay', [StripePaymentController::class, 'stripePost'])->name('payment.post');

require __DIR__.'/auth.php';
