<?php

namespace Database\Seeders;

use App\Models\CalculationType;
use App\Models\ProductType;
use Illuminate\Database\Seeder;

class CalculationTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'with_tax',
            ],
            [
                'name' => 'without_tax',
            ]
        ];

        return CalculationType::insert($data);
    }
}
