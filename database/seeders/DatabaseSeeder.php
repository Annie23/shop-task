<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductTypeSeeder::class);

        \App\Models\Product::factory(30)->create();
        \App\Models\ProductImage::factory(30)->create();
    }
}
