<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name;
        $slug = Str::slug($name, "-");

        return [
            'product_type_id' => $this->faker->numberBetween(1, 2),
            'name' => $name,
            'price' => $this->faker->numberBetween(4000, 7000),
            'slug' => $slug,
            'description' => $this->faker->realText(120),
            'status' => 1,
        ];
    }
}
