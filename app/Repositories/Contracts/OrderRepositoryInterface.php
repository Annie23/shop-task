<?php


namespace App\Repositories\Contracts;


interface OrderRepositoryInterface
{
    public function orderProducts($params);

    public function updateOrderStatus($id);

    public function getOrders();
}
