<?php


namespace App\Repositories\Contracts;


interface BasketRepositoryInterface
{
    public function modelClass();

    public function listProductsInBasket();

    public function addProductToBasket($params);
}
