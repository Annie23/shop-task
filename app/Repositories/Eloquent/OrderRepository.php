<?php


namespace App\Repositories\Eloquent;



use App\Models\Order;
use App\Repositories\Contracts\OrderRepositoryInterface;

class OrderRepository extends AbstractRepository implements OrderRepositoryInterface
{
    public function orderProducts($params)
    {
        $order = $this->model->create([
            'user_id' => auth()->id(),
            'status' => Order::STATUS['PENDING']
        ]);

        if ($order) {
            foreach ($params['products'] as $productId) {
                $order->orderItems()->create([
                    'product_id' => 2,
                    'quantity' => 1, //todo send from front
                    'price' => 1000, //todo
                ]);
            }
        }

        return $order;
    }

    public function updateOrderStatus($id)
    {
        $order = $this->model->findOrFail($id);
        $order->status = Order::STATUS['CONFIRMED'];

        return $order->save();
    }

    public function getOrders()
    {
        return $this->model->where('user_id', auth()->id())
            ->with(['orderItems' => function($q) {
                $q->with(['product' => function($q) {
                    $q->with('productImages');
                }]);
            }])
            ->paginate($this->limit);
    }
}
