<?php


namespace App\Repositories\Eloquent;



abstract class AbstractRepository
{
    protected $model;
    protected $limit = 20;

    public function __construct($model = null)
    {
        $this->model = $model;
    }

}
