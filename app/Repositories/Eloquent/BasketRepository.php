<?php


namespace App\Repositories\Eloquent;


use App\Models\Basket;
use App\Repositories\Contracts\BasketRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BasketRepository  extends AbstractRepository implements BasketRepositoryInterface
{
    public function modelClass():Model
    {
        return new Basket();
    }

    public function listProductsInBasket()
    {
        return $this->model->where('user_id', auth()->id())->with('product')->paginate($this->limit);
    }

    public function addProductToBasket($params)
    {
        return $this->model->create([
            'user_id' => auth()->id(),
            'product_id' => $params['product_id'],
        ]);
    }
}
