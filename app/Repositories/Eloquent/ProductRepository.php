<?php


namespace App\Repositories\Eloquent;


use App\Repositories\Contracts\ProductRepositoryInterface;

class ProductRepository extends AbstractRepository implements ProductRepositoryInterface
{
    public function getProducts()
    {
        return $this->model->with('productImages')->paginate($this->limit);
    }
}
