<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\CreateOrderRequest;
use App\Services\Contracts\OrderServiceInterface;

class OrderController extends Controller
{

    public $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.orders', ['orders' => $this->orderService->getOrders()]);
    }

    public function store(CreateOrderRequest $request)
    {
        return view('pages.checkout', ['order' => $this->orderService->orderProducts($request->validated())]);
    }
}
