<?php

namespace App\Http\Controllers;

use App\Services\Contracts\ProductServiceInterface;

class ProductController extends Controller
{
    public $productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        $products = $this->productService->getProducts();

        return view('dashboard', compact('products'));
    }
}
