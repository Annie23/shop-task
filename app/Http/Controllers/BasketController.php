<?php

namespace App\Http\Controllers;

use App\Http\Requests\Basket\CreateBasketRequest;
use App\Services\Contracts\BasketServiceInterface;
use Illuminate\Http\Response;

class BasketController extends Controller
{
    public $basketService;

    public function __construct(BasketServiceInterface $basketService)
    {
        $this->basketService = $basketService;
    }

    public function index()
    {
        return view('pages.basket', ['baskets' => $this->basketService->listProductsInBasket()]);
    }


    public function create(CreateBasketRequest $request)
    {
        return response()->json($this->basketService->addProductToBasket($request->validated()), Response::HTTP_OK);
    }
}
