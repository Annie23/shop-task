<?php

namespace App\Http\Controllers;

use App\Services\Contracts\OrderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;

class StripePaymentController extends Controller
{
    public $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    public function pay()
    {
        return view('stripe');
    }

    public function stripePost(Request $request)
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $charge = Charge::create ([
            "amount" => 100 * 100,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "Test payment"
        ]);

        $order = false;

        if ($charge) {
            $order = $this->orderService->updateOrderStatus($request->order_id);
            Session::flash('success', 'Payment successful!');
        }

        if (!$order) {
            Session::flash('danger', 'Payment not processed!');
        }

        return redirect()->route('basket.index');
    }
}
