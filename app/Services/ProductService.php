<?php


namespace App\Services;


use App\Services\Contracts\ProductServiceInterface;

class ProductService extends AbstractService implements ProductServiceInterface
{
    public function getProducts()
    {
        return $this->repository->getProducts();
    }
}
