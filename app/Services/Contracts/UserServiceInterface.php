<?php


namespace App\Services\Contracts;



interface UserServiceInterface extends AbstractServiceInterface
{
    public function list();

    public function add($params);
}
