<?php


namespace App\Services\Contracts;



interface OrderServiceInterface extends AbstractServiceInterface
{
    public function orderProducts($params);

    public function updateOrderStatus($id);

    public function getOrders();
}
