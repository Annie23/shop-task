<?php


namespace App\Services\Contracts;


interface ProductServiceInterface extends AbstractServiceInterface
{
    public function getProducts();
}
