<?php


namespace App\Services\Contracts;


interface BasketServiceInterface extends AbstractServiceInterface
{
    public function listProductsInBasket();

    public function addProductToBasket($params);
}
