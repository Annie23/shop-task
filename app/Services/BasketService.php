<?php


namespace App\Services;


use App\Services\Contracts\BasketServiceInterface;

class BasketService extends AbstractService implements BasketServiceInterface
{

    public function listProductsInBasket()
    {
        return $this->repository->listProductsInBasket();
    }

    public function addProductToBasket($params)
    {
        return $this->repository->addProductToBasket($params);
    }
}
