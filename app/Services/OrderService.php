<?php


namespace App\Services;


use App\Services\Contracts\OrderServiceInterface;

class OrderService extends AbstractService implements OrderServiceInterface
{
    public function orderProducts($params)
    {
        return $this->repository->orderProducts($params);
    }

    public function updateOrderStatus($id)
    {
        return $this->repository->updateOrderStatus($id);
    }

    public function getOrders()
    {
        return $this->repository->getOrders();
    }
}
