<?php


namespace App\Services;


use App\Services\Contracts\AbstractServiceInterface;

class AbstractService implements AbstractServiceInterface
{
    protected $repository;

    public function __construct($repository)
    {
        $this->repository = $repository;
    }
}
