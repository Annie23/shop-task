<?php


namespace App\Services;


use App\Repositories\Contracts\BasketRepositoryInterface;
use App\Services\Contracts\UserServiceInterface;

class UserService extends AbstractService implements UserServiceInterface
{
    public $repo;

    public function __construct(BasketRepositoryInterface $basketRepository)
    {
        $this->repo = $basketRepository;
    }

    public function list()
    {
        return $this->repo->listProductsInBasket();
    }

    public function add($params)
    {
        return $this->repo->addProductToBasket($params);
    }
}
