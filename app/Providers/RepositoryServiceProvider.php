<?php

namespace App\Providers;

use App\Models\Basket;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Repositories\Contracts\BasketRepositoryInterface;
use App\Repositories\Contracts\OrderRepositoryInterface;
use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Eloquent\BasketRepository;
use App\Repositories\Eloquent\OrderRepository;
use App\Repositories\Eloquent\ProductRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Services\BasketService;
use App\Services\Contracts\BasketServiceInterface;
use App\Services\Contracts\OrderServiceInterface;
use App\Services\Contracts\ProductServiceInterface;
use App\Services\Contracts\UserServiceInterface;
use App\Services\OrderService;
use App\Services\ProductService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories();
        $this->registerServices();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function registerRepositories()
    {
        $this->app->singleton( UserRepositoryInterface::class, function() {
            return new UserRepository(new User());
        });

        $this->app->singleton( ProductRepositoryInterface::class, function() {
            return new ProductRepository(new Product());
        });

        $this->app->singleton( BasketRepositoryInterface::class, function() {
            return new BasketRepository(new Basket());
        });

        $this->app->singleton( OrderRepositoryInterface::class, function() {
            return new OrderRepository(new Order());
        });
    }

     private function registerServices()
    {
        $this->app->bind(UserServiceInterface::class, function () {
            return new UserService($this->app->make(UserRepositoryInterface::class));
        });
        $this->app->bind(ProductServiceInterface::class, function () {
            return new ProductService($this->app->make(ProductRepositoryInterface::class));
        });
        $this->app->bind(BasketServiceInterface::class, function () {
            return new BasketService($this->app->make(BasketRepositoryInterface::class));
        });
        $this->app->bind(OrderServiceInterface::class, function () {
            return new OrderService($this->app->make(OrderRepositoryInterface::class));
        });
    }
}
