<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container" style="margin-top: 80px">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Shop</li>
                            </ol>

                            <a href="{{route('basket.index')}}" class="float-right">
                                <i class="fa fa-shopping-cart"></i> Basket
                            </a>

                            <a href="{{route('order.index')}}" class="float-right">
                                <i class=""></i> Orders History
                            </a>
                        </nav>
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <h4>Products In Our Store</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    @isset($products)
                                        @foreach($products as $product)
                                        <div class="col-lg-3">
                                            <div class="card" style="margin-bottom: 20px; height: auto;">
                                                @isset($product->productImages->first()->path)
                                                    <img src="{{ $product->productImages->first()->path }}"
                                                         class="card-img-top mx-auto"
                                                         style="height: 150px; width: 150px;display: block;"
                                                         alt="img"
                                                    >
                                                @endisset
                                                <div class="card-body">
                                                    <a href=""><h6 class="card-title">{{ $product->name }}</h6></a>
                                                    <p>${{ $product->price }}</p>
                                                    <form action="" method="POST">
                                                        {{ csrf_field() }}
                                                        <div class="card-footer" style="background-color: white;">
                                                            <div class="row">
                                                                <button class="btn btn-secondary btn-sm tooltip-test" onclick="addToBasket({{$product->id}}, event)" title="add to cart">
                                                                    <i class="fa fa-shopping-cart"></i> add to basket
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    function addToBasket(productId, event) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        event.preventDefault();

        $.ajax({
            url: "{{ route('basket.add') }}",
            type:"POST",
            data:{
                product_id:productId
            },
            success:function(response){
                console.log(response);
                if(response) {
                    alert('product added to basket');
                }
            },
        });
    }
</script>
