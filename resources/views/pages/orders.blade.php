<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Orders History') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container" style="margin-top: 80px">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Shop</li>
                            </ol>
                        </nav>
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <h4></h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    @isset($orders)
                                        <div class="row">
                                            {{dd($orders)}}
                                        </div>
                                    @endisset

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    function buy(event) {

        let products = [];

        $("input:checkbox[name=product]:checked").each(function () {
            products.push($(this).val());
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        event.preventDefault();

        $.ajax({
            url: "{{ route('order.store') }}",
            type: "POST",
            data: {
                product_id: productId
            },
            success: function (response) {
                if (response) {
                    alert('product added to basket');
                }
            },
        });
    }
</script>
