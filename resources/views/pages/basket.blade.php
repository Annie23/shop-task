<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Basket') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container" style="margin-top: 80px">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Shop</li>
                            </ol>
                        </nav>
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <h4>Products You have add to basket</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    @isset($baskets)
                                        <div class="row">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="row">Select</th>
                                                    <th scope="row">Quantity</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Image</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <form action="{{route('order.store')}}" method="POST">
                                                    {{ csrf_field() }}
                                                    @foreach($baskets as $basket)

                                                        <tr>
                                                            <th scope="row">{{ $basket->product->id }}</th>
                                                            <th scope="row">
                                                                <input
                                                                    name="products[]"
                                                                    value="{{$basket->product->id}}"
                                                                    type="checkbox"
                                                                >
                                                            </th>
                                                            <th scope="row">1</th>
                                                            <td>{{ $basket->product->name }}</td>
                                                            <td>{{ $basket->product->price }}</td>
                                                            <td>
                                                                @isset($basket->product->productImages->first()->path)
                                                                    <img
                                                                        src="{{ $basket->product->productImages->first()->path }}"
                                                                        class="card-img-top mx-auto"
                                                                        style="height: 150px; width: 150px;display: block;"
                                                                        alt="img"
                                                                    >
                                                                @endisset</td>
                                                        </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            <button type="submit" class="col-md-4 btn btn-secondary tooltip-test btn-lg ml-60"
{{--                                                    onclick="buy(event)"--}}
                                                    title="buy">
                                                Buy
                                            </button>

                                            </form>

                                        </div>
                                </div>
                            </div>
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    function buy(event) {

        let products = [];

        $("input:checkbox[name=product]:checked").each(function () {
            products.push($(this).val());
        });
        console.log(products)
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        event.preventDefault();

        $.ajax({
            url: "{{ route('order.store') }}",
            type: "POST",
            data: {
                product_id: productId
            },
            success: function (response) {
                if (response) {
                    alert('product added to basket');
                }
            },
        });
    }
</script>
